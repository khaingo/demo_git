#include <iostream>
#include <windows.h>
#include <time.h>
#include <conio.h>
#include "console.h"

#define consoleWidth 60
#define consoleHight 25

using namespace std;

enum TrangThai {UP, DOWN, LEFT, RIGHT};

typedef struct
{
    int x, y;
}ToaDo;

typedef struct
{
    ToaDo dot[25*60];
    int n;
    int diem;
    TrangThai tt;
}Snake;

void khoiTao (Snake &snake, ToaDo &hq)
{
    snake.n = 2;

    snake.diem = 0;

    snake.dot[0].x = 1;
    snake.dot[0].y = 0;
    snake.dot[1].x = 0;
    snake.dot[1].y = 0;

    snake.tt = RIGHT;

    hq.x = 2;
    hq.y = 0;
}

void hienThi (Snake snake, ToaDo hq)
{
    clrscr();

    //Hien thi hoa qua
    gotoXY(hq.x, hq.y);
    cout << (char)3;

    //Hien thi ran
    for(int i = 0; i < snake.n; i++)
    {
        gotoXY(snake.dot[i].x, snake.dot[i].y);
        if(i == 0)
            cout << (char)2;
        else
            cout << "*";
    }

    //Hien thi tuong
    for(int y = 0; y < consoleHight; y++)
    {
        gotoXY(consoleWidth, y);
        cout << (char)178;
    }

    //Hien thi diem
    gotoXY(consoleWidth + 2, 0);
    cout << "Score : " << snake.diem;
}

void dieuKhien_va_DiChuyen(Snake &snake)
{
    for(int i = snake.n - 1; i > 0; i--)
        snake.dot[i] = snake.dot[i-1];

    if(_kbhit())
    {
        int key = _getch();

        if((key == 'A' || key == 'a') && snake.tt != RIGHT)
            snake.tt = LEFT;
        else if((key == 'D' || key == 'd') && snake.tt != LEFT)
            snake.tt = RIGHT;
        else if((key == 'S' || key == 's') && snake.tt != UP)
            snake.tt = DOWN;
        else if((key == 'W' || key == 'w') && snake.tt != DOWN)
            snake.tt = UP;
    }


    if (snake.tt == UP)
        snake.dot[0].y--;
    else if (snake.tt == DOWN)
        snake.dot[0].y++;
    else if (snake.tt == RIGHT)
        snake.dot[0].x++;
    else if (snake.tt == LEFT)
        snake.dot[0].x--;
}

int xuLy(Snake &snake, ToaDo &hq, int &tGian)
{
    //rắn dài ra ở phía đuôi
    if(snake.dot[0].x == hq.x && snake.dot[0].y == hq.y)
    {
        snake.n++;

        if(snake.dot[snake.n - 2].x == snake.dot[snake.n - 3].x && snake.dot[snake.n - 2].y < snake.dot[snake.n - 3].y)
        {
            snake.dot[snake.n - 1].x = snake.dot[snake.n - 2].x;

            if(snake.dot[snake.n - 2].y < snake.dot[snake.n - 3].y)
                snake.dot[snake.n - 1].y = snake.dot[snake.n - 2].y - 1;
            else
                snake.dot[snake.n - 1].y = snake.dot[snake.n - 2].y + 1;
        }
        else if(snake.dot[snake.n - 2].y == snake.dot[snake.n - 3].y)
        {
            snake.dot[snake.n - 1].y = snake.dot[snake.n - 2].y;

            if(snake.dot[snake.n - 2].x < snake.dot[snake.n - 3].x)
                snake.dot[snake.n - 1].x = snake.dot[snake.n - 2].x - 1;
            else if(snake.dot[snake.n - 2].x > snake.dot[snake.n - 3].x)
                snake.dot[snake.n - 1].x = snake.dot[snake.n - 2].x + 1;
        }

        snake.diem++;

        hq.x = rand() % consoleWidth;
        hq.y = rand() % consoleHight;

        if(tGian > 10)
            tGian -= 5;
    }

    if(snake.dot[0].x < 0 || snake.dot[0].x >= consoleWidth ||
       snake.dot[0].y < 0 || snake.dot[0].y >= consoleHight)
        return -1;

    for(int i = 1; i < snake.n; i++)
        if(snake.dot[0].x == snake.dot[i].x && snake.dot[0].y == snake.dot[i].y)
            return -1;

    return 0;
}

int main()
{
    srand(time(NULL));
    Snake snake;
    ToaDo hq;
    int tGian = 200;
    int ma;

    khoiTao(snake, hq);

    while(1)
    {
        hienThi(snake, hq);

        dieuKhien_va_DiChuyen(snake);

        ma = xuLy(snake, hq, tGian);
        if(ma == -1)
        {
            //
            gotoXY(30, 10);
            cout << "Your score is " << snake.diem;
            gotoXY(21, 12);
            cout << "Press 'Q' to replay; 'E' to end game";

            if(_kbhit())
            {
                int key = _getch();

                if(key == 'q' || key == 'Q')
                {
                    ma = 1;

                    khoiTao(snake, hq);

                    main();
                }
                else if(key == 'e' || key == 'E')
                    return 0;
            }
        }

        Sleep(tGian);
    }
}

